"use strict";


(function() {
	$(window).on('action:ajaxify.end', function(event, data) {
		$(document).ready(function() {

			if (new RegExp(/^category\/[0-9]+/).test(data.url)) {
				var uid = app.user.uid;
				$('a[data-sort="newest_to_oldest"]').parent().parent().append('<li><a href="#" class="most_views" data-sort="most_views"><i class="fa fa-fw"></i> Most Views</a></li>');
				socket.emit('plugins.connectordinamentomostviews.getClickMostViews', {
					'uid': uid
				}, function(err, result) {
					if (err) {
						app.alert(err);
					}

					if (result) {
						$('a[data-sort="most_views"] i').addClass('fa-check');
						//$('a[data-sort="newest_to_oldest"]').parent().parent().append('<li><a href="#" class="most_views" data-sort="most_views"><i class="fa fa-fw fa-check"></i> Most Views</a></li>');

					}
				});


				$(document).on('click', 'a[data-sort]', function(e) {

					if ($(this).is('[data-sort="most_views"]')) {

						socket.emit('plugins.connectordinamentomostviews.inserisciClickMostViews', {
							'uid': uid
						}, function(err) {

						});


					} else {
						var attributo = $(this).attr('data-sort');

						socket.emit('plugins.connectordinamentomostviews.deleteClickMostViews', {
							'uid': uid
						}, function(err) {


						});

					}

					//$(this).children('i').addClass("fa-check");
				});

				//$(this).children('i').addClass("fa-check");

			}
		});
	});

}());