"use strict";

var converter = {};
var user = require.main.require('./src/user');
var db = require.main.require('./src/database'),
	plugins = require.main.require('./src/plugins'),
	async = require.main.require('async'),
	winston = require.main.require('winston'),
	Topics = require.main.require('./src/topics'),
	categories = require.main.require('./src/categories'),
	sockets = require('./lib/sockets'),
	SocketPlugins = require.main.require('./src/socket.io/plugins');


SocketPlugins.connectordinamentomostviews = sockets;
var id_plugin_inserisci_data = "nodebb-plugin-connect-inserisci-data-fissa-discussione";

var hash = "connect-inserisci-data-fissa-discussione:";

converter.riordinaCategoria = function(data, callback) {

	user.getSettings(data['uid'], function(err, settings) {
		if (settings.categoryTopicSort === 'most_views') {
			//Non posso ordinare in questa maniera perchè se fisso una discussione non va in alto
			//data.reverse = false;
			//data.set = 'cid:' + data.cid + ':tids:posts';
			//winston.verbose("[connect-ordina-categorie-per-zero-risposte] scelto l'ordinamento dei topics per zero risposte");

			//most_views = true;
			//data['most_views']=true;


			var topics = data.topics;
			var array_pin = [];
			var array_non_pin = [];
			var array_sistema_pin = [];
			plugins.isActive(id_plugin_inserisci_data, function(err, attivo) {
				if (err) {
					return callback(null, err);
				}
				if (attivo == false) {
					async.waterfall([
							function(next) {
								topics.sort(function(a, b) {

									if (a.viewcount > b.viewcount) {
										return -1;
									}
									if (a.viewcount < b.viewcount) {
										return 1;
									}
									// a must be equal to b
									return 0;


								});

								//console.log("TOPICS ORDINATI "+JSON.stringify(topics));
								return next(null, topics);
							},
							function(topics_ordinati, next) {

								async.parallel({
									pin: function(prossima) {

										for (var i = 0; i < topics_ordinati.length; i++) {
											if (topics_ordinati[i].pinned) {
												array_pin.push(topics_ordinati[i]);
											}
										}
										return prossima(null, array_pin);
									},
									non_pin: function(prossima) {

										for (var i = 0; i < topics_ordinati.length; i++) {
											if (!topics_ordinati[i].pinned) {
												array_non_pin.push(topics_ordinati[i]);
											}
										}
										return prossima(null, array_non_pin);

									}
								}, function(err, array_pinned) {
									if (err) {
										return callback(err);
									}

									var array_sistema_pin = array_pinned.pin.concat(array_pinned.non_pin);
									//data.topics = array_sistema_pin;
									return next(null, array_sistema_pin);
								});


							}
						],
						function(err, topics_ordinati) {
							if (err) {
								return callback(err);
							}

							data.topics = topics_ordinati;
							//console.log("HERE " + JSON.stringify(data.topics));
							return callback(null, data);

						});
				} else {

					async.waterfall([
							function(next) {
								topics.sort(function(a, b) {

									if (a.viewcount > b.viewcount) {
										return -1;
									}
									if (a.viewcount < b.viewcount) {
										return 1;
									}
									// a must be equal to b
									return 0;

								});
								//console.log("TOPICS ORDINATI "+JSON.stringify(topics));
								return next(null, topics);
							},
							function(topics_ordinati, next) {

								async.parallel({
									//Togliere prossima qui
									pin: function(avanti) {

										async.each(topics, function(topicID, continua) {
											async.waterfall([
												function(prossima) {
													db.getObject(hash + topicID.tid, prossima);
												},
												function(fissaDiscussione, prossima) {
													//console.log("ASYNCCc TOPIC " + topicID.tid + " " + topicID.pinned);
													//for (var i = 0; i < topics_ordinati_per_zero_risposte.length; i++) {
													if (topicID.pinned) {

														var timestamp_corrente = Date.now();
														var tid = topicID.tid;
														if (!fissaDiscussione || fissaDiscussione.timestamp_iniziale == 0) {

															array_pin.push(topicID);
															return prossima();
														}


														if (fissaDiscussione) {
															if (fissaDiscussione.timestamp_iniziale == 0) {
																array_pin.push(topicID);
																return prossima();
															}
															if (timestamp_corrente > (parseInt(fissaDiscussione.timestamp_iniziale)) && timestamp_corrente < parseInt(fissaDiscussione.timestamp_finale)) {

																array_pin.push(topicID);
																return prossima();
															} else {
																//QUando è pinned ma il tempo è scaduto lo devo eliminare dal database
																array_non_pin.push(topicID);

																//return prossima();
																async.waterfall([
																	function(avanti) {
																		Topics.setTopicField(tid, ['pinned'], false, avanti);

																	},
																	function(avanti) {
																		db.delete(hash + tid, avanti);
																	}
																], function(errore) {
																	if (errore) {
																		return callback(errore);
																	}
																	return prossima();
																});



															}
														}
													}
													return prossima();

												}


											], function(errore) {
												if (errore) {
													return callback(errore);
												}
												continua();

											});
										}, function(errore) {
											if (errore) {
												return callback(errore);
											}
											//console.log("PRIMA DI TORNARE"+ JSON.stringify(array_pin));
											return avanti(null, array_pin);
										});

									},
									non_pin: function(avanti) {

										for (var i = 0; i < topics.length; i++) {
											if (!topics[i].pinned) {
												array_non_pin.push(topics[i]);
											}
										}
										return avanti(null, array_non_pin);

									}
								}, function(err, array_pinned) {
									if (err) {
										return callback(err);
									}
									//console.log("ARRAY SUSTEMA IN " + JSON.stringify(array_pinned));

									array_sistema_pin = array_pinned.pin.concat(array_pinned.non_pin);

									return next(null, array_sistema_pin);
								});


							}
						],
						function(err, topics_ordinati) {
							if (err) {

								return callback(err);
							}

							data.topics = topics_ordinati;
							return callback(null, data);

						});
				}

			});
		} else {
			return callback(null, data);
		}

	});


};



module.exports = converter;